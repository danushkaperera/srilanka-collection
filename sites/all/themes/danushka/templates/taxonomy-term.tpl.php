 
	  <?php
	  $view = views_get_current_view();
$tid = $view->args[0];
if ( is_numeric( $tid ) ) {
  $term = taxonomy_term_load($tid);
  if (module_exists('i18n_taxonomy') && i18n_taxonomy_vocabulary_mode($term->vid) == I18N_MODE_LOCALIZE) {
    $description = i18n_string("taxonomy:term:$term->tid:description", $term->description);
  } else {
    $description = $term->description;
  }
  print $description;
}?>
