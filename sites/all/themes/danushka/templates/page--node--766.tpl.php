    <div class="hotel-page">
  <div class="rownew">
     <div class="clearfix"></div>
     <div id="head1" style="">
        <?php print render($page['hotel_banner']) ?> 
     </div>
       <div class="hotel-color-bar"> </div>
     </div>
   <br>
   
        <div class="container">
      <div class="clear"> </div>
      <div class="hotel-mid-content"> 
        <div class="hotel-location-img"><?php print render($page['hotel_location']) ?>  </div>
        <div class="hotel-main-content"><?php print render($page['hotel_main_content']) ?>  </div>
        <div class="hotel-key-section"><h3> Key Facts </h3><?php print render($page['hotel_key']) ?>  </div>
      </div>
       
      <div class="clear"> </div>
         </div>
        <br>
          <div class="hotel-color-bar"> </div>
        
       <div class="container">
    
      <div class="hotel-left"> <?php print render($page['content']) ?> </div>
     <div class="hotel-right"> <?php print render($page['image_gallery']) ?>  </div>
      </div>

    
   </div>
  
  