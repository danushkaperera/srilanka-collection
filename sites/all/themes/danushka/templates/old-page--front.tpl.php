<!doctype html>
<html>

<head>
<!--  
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>New</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">


    HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<?php
function mytheme_preprocess_html(&$variables) {
 //drupal_add_css('http://fonts.googleapis.com/css?family=Lato:400,300,700', array('type' => 'external'));
}
?>



<?php
//drupal_add_js('https://code.jquery.com/jquery-1.10.2.min.js', 'external');
?>

<?php
function example_preprocess_html(&$variables) {
  $options = array(
    'group' => JS_THEME,
  );
 // drupal_add_js(drupal_get_path('theme', 'danushka'). '/js/bootstrap.min.js', $options);
}
?>



<?php
//drupal_add_js(drupal_get_path('theme', 'danushka') .'/js/bootstrap.min.js');
?>


</head>

<body>
   
    <div class="main-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
					
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/main1banner.jpg"/>
                                </div>
                               
                                <div class="item">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/main2banner.jpg"/>
                                </div>
                                
                                <div class="item">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/main3banner.jpg"/>
                                </div>
                                                        <div class="item">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/main4banner.jpg"/>
                                </div>
                                                        <div class="item">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/main5banner.jpg"/>
                                </div>
                                                        <div class="item">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/main6banner.jpg"/>
                                </div>
                                                        <div class="item">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/banner7main.jpg"/>
                                </div>
                                                        <div class="item">
                                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/banner8main.jpg"/>
                                </div>


                            </div>
                           

                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left">
                                    <img src="<?php print base_path(). path_to_theme() ?>/images/banner-left.png"/>
                                </span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right">
                                    <img src="<?php print base_path(). path_to_theme() ?>/images/banner-right.png"/>
                                </span>
                            </a>


                        </div>
						
                    </div>
                </div>
            </div>
        </div>
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="disc">
                         <?php print render($page['content']) ?>
                       </div>
                </div>
            </div>
        </div>
    </div>
   
</body>

</html>
