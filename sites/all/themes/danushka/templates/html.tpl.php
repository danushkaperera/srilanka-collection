<!DOCTYPE html>
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>

<?php print $scripts; ?>



<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://srilankacollection.com/sites/all/themes/danushka/js/jquery.rwdImagemaps.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53905420-1', 'auto');
  ga('send', 'pageview');

</script>
  <script>
  $(document).ready(function(){

 $( ".imageblock-image" ).addClass( "img-responsive" );
 
  //$( "img" ).addClass( "img-responsive" );
});</script>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body class="<?php print $classes; ?>"<?php print $attributes; ?>>
  
    <header style="padding: 30px 0 15px 0;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5 pull-left text-center">
                    <a href="http://www.srilankacollection.com" title="<?php print t('Home'); ?>" rel="home" id="logo">
                    <img class="img-responsive" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/logo.png" alt="<?php print t('Home'); ?>" />
                      </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 pull-news">
                    <div class="news-sub">
                         <a href="http://www.srilankacollection.com/subscribe" title="<?php print t('Subscribe'); ?>" rel="home" id="logo">
                    <img class="img-responsive normal " alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/news.png"/> 
                      </a>
      
                        
                    </div>
                </div>
                 <div class="col-xs-12 col-sm-6 col-md-1 pull-fb">
   <a href="https://www.facebook.com/srilankacollection" target="_blank"> <img class="img-responsive normal fbicon" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/fb.jpg"  />
   </a>

                 </div>
            </div>
        </div>
    </header>
    <div class="main-container main-menu-nim" style="z-index: 10000; position: relative; ">
        <div class="main-menu">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
							<div class="clear" > </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                


								<?php 
          if (module_exists('i18n')) {
            $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
          } else {
            $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
          }
          print drupal_render($main_menu_tree);
          ?>
						
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>


  
  <?php print $page_top; ?>
  
  <?php print $page; ?>
  <?php// print $page_bottom; ?>
  
 
   <footer style="padding:10px 0 0 0">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                <p> © 2014 Sri Lanka Collection (Pvt) Ltd. All rights reserved</p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="social">
					<?php /*	<a href="https://www.twitter.com"> <img class="img-responsive normal" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/tw.jpg"  /> */ ?>
						</a>
         
                     <?php /*   <a href="https://plus.google.com"> <img class="img-responsive normal" alt="Image" src="<?php print base_path(). path_to_theme() ?>/images/gp.jpg" />  */ ?>
                      </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  
  </body>

</html>
