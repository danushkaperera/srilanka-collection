  <link href="<?php print base_path(). path_to_theme() ?>/css/vilas.css" type="text/css" rel="stylesheet" />
<div class="map padd0">
    <?php print render($page['banner']) ?> 
  <div id="zoom">

  <div id="pin-16" class="box16 zoomimg">
                           <p id="tit16">Camellia Hills, near Dickoya</p>
                            <div class="pin-text16">
                                <a  href="http://www.srilankacollection.com/camellia-hills-bungalow" >  
                                        <span class="description des9">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/Cameilia_hills.png" >
                                       </span>
                                 </a> 
                            </div>
                  </div>

                <div id="pin-2" class="box2 zoomimg">
                           <p id="tit02">Apsara, Bentota</p>
                            <div class="pin-text2 ">
                                <a  href="http://www.srilankacollection.com/apsara-villa-near-bentota" >  
                                        <span class="description des9">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/apsara.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-17" class="box17 zoomimg">
                           <p id="tit17">Ivory House, Angulugaha</p>
                            <div class="pin-text17 ">
                                <a  href="http://www.srilankacollection.com/node/642" >  
                                        <span class="description des9">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/ivoryhouse.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
                      <div id="pin-3" class="box3 zoomimg">
                           <p id="tit03">Why House, Mihiripenna, near Galle</p>
                            <div class="pin-text3 ">
                                <a  href="http://www.srilankacollection.com/node/563" >  
                                        <span class="description des3">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/WhyHouse-01.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-4" class="box4 zoomimg">
                           <p id="tit04">41 Lighthouse Street Villa, Galle Fort</p>
                            <div class="pin-text4 ">
                                <a  href="http://srilankacollection.com/41-lighthouse-street-villa" >  
                                        <span class="description des4">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/41_Thumbnails.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-5" class="box5 zoomimg">
                           <p id="tit05">Mandalay Lake Villa, Koggala Lake</p>
                            <div class="pin-text5 ">
                                <a  href="http://srilankacollection.com/mandalay-lake-villa" >  
                                        <span class="description des5">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/new-mandalay-villa-lake.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
                      <div id="pin-18" class="box18 zoomimg">
                           <p id="tit18">Uda Kanda Villa, Kaduruduwa, near Galle</p>
                            <div class="pin-text18 ">
                                <a  href="http://srilankacollection.com/uda-kanda-villa" >  
                                        <span class="description des5">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/uda_Thumbnail.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-6" class="box6 zoomimg">
                           <p id="tit06">Suriyawatta, Weligama</p>
                            <div class="pin-text6 ">
                                <a  href="http://www.srilankacollection.com/suriyawatta-near-weligama" >  
                                        <span class="description des6">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/suriyawatta.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-7" class="box7 zoomimg">
                           <p id="tit07">Ranawara, Kahandamodara</p>
                            <div class="pin-text7 ">
                                <a  href="http://www.srilankacollection.com/ranawara-kahandamodera-near-tangalle" >  
                                        <span class="description des7">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/ranawara.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-8" class="box8 zoomimg">
                           <p id="tit08">The Walatta House, Tangalle</p>
                            <div class="pin-text8 ">
                                <a  href="http://www.srilankacollection.com/walatta-house-near-tangalle" >  
                                        <span class="description des8">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/walatta.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-9" class="box9 zoomimg">
                           <p id="tit09">Claughton House, Dickwella</p>
                            <div class="pin-text9 ">
                                <a  href="http://www.srilankacollection.com/claughton-house-dickwella" >  
                                        <span class="description des9">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/claughton.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-10" class="box10 zoomimg">
                           <p id="tit010">Pointe Sud, Mirissa</p>
                            <div class="pin-text10 ">
                                <a  href="http://www.srilankacollection.com/pointe-sud-near-mirissa" >  
                                        <span class="description des10">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/pointe-sud.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
<div id="pin-11" class="box11 zoomimg">
                           <p id="tit011">Villa Kumara, near Weligama</p>
                            <div class="pin-text11 ">
                                <a  href="http://srilankacollection.com/villa-kumara-near-weligama" >  
                                        <span class="description des11">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/villa-kumar.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
        <div id="pin-12" class="box12 zoomimg">
                           <p id="tit012">Skye House, Habaraduwa</p>
                            <div class="pin-text12 ">
                                <a  href="http://www.srilankacollection.com/skye-house-Habaraduwa" >  
                                        <span class="description des12">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/skye-house-new.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
 <div id="pin-14" class="box14 zoomimg">
                           <p id="tit14">Taylors Hill, Deltota, near Kandy </p>
                            <div class="pin-text14 ">
                                <a  href="http://www.srilankacollection.com/taylors-hill-villa" >  
                                        <span class="description des14">   
                                           <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/TaylorsHill-02.png" >                     
                                       </span>
                                 </a> 
                            </div>
                      </div>
 <div id="pin-15" class="box15 zoomimg">
                           <p id="tit15">The Last House, Mawella, near Tangalle </p>
                            <div class="pin-text15 ">
                                <a  href="http://www.srilankacollection.com/node/561" >  
                                        <span class="description des15">                        
                                             <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/villas/Thelasthouse-03.png" >         
                                       </span>
                                 </a> 
                            </div>
                      </div>
<!--  
                 //Suriywatta
                 //Walatta
                      <div id="pin-5" class="box5 zoomimg">
                            <div class="pin-text5 ">
                                <a  href="" >  
                                        <span class="description ">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/popup5.png" >
                                                      </span>
                                 </a> 
                            </div>
                      </div>
                      <div id="pin-6" class="box6 zoomimg">
                            <div class="pin-text6 ">
                                <a  href="" >  
                                        <span class="description ">                        
                                              <img class="image"src="<?php print base_path(). path_to_theme() ?>/images/popup5.png" >
                                       </span>
                                 </a> 
                            </div>
                      </div>
     -->
  </div>
  </div>
               <div class="clearfix"></div>  
    <div class="container-fluid" >
          <div class="container"> 
          <div class="col-md-12">  
 <?php print render($page['content']) ?> 
          </div>
          </div>
    </div>
</div>